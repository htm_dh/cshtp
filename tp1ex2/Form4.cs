﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace tp1ex2
{
    
    public partial class Form4 : Form
    {
        List<Label> LTxtC = new List<Label>();
        List<TextBox> LTxtP = new List<TextBox>();
        int nbJeux = 0;
        bool animation = false;
        int n1, n2, n3, n4;
        Random ra = new Random();
        private static Color f = ColorTranslator.FromHtml("#0078D7");
        private void button6_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
                LTxtC[i].ForeColor = f;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 4; i++)
            {
                LTxtP[i].BackColor = Color.White;
                LTxtP[i].Text = "";
            }
            LTxtP[0].Focus();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int i, j, correct = 0;
            for (i = 0;i<4;i++)
            {
                if (LTxtP[i].Text == LTxtC[i].Text)
                {
                    LTxtP[i].BackColor = Color.Lime;
                    correct++;
                }
                else
                    for (j = 0; j < 4; j++)
                    {
                        if (LTxtP[i].Text == LTxtC[j].Text)
                        {
                            LTxtP[i].BackColor = Color.Yellow;
                            break;
                        }
                    }
            }
            if (correct == 4)
            {
                timer1.Start();
                for (i = 0; i < 4; i++)
                    LTxtC[i].ForeColor = Color.Black;
            }
            else
            {
                nbJeux++;
                LblNbJeux.Text = (nbJeux + 1).ToString();
                if (nbJeux == 3)
                {
                    for (i = 0; i < 4; i++)
                        LTxtC[i].ForeColor = Color.Black;
                    MessageBox.Show("Vous avez perdu", "Game Over",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    button1.Enabled = false;
                    button2.Enabled = false;
                    button3.Enabled = false;
                    button4.Enabled = false;
                    for (i = 0; i < 4; i++)
                        LTxtC[i].Text = "";
                }
            }


        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (animation == true)
            {
                for (int i = 0; i < 4; i++)
                    LTxtC[i].BackColor = Color.Yellow;
                animation = false;
            }
            else
            {
                for (int i = 0; i < 4; i++)
                    LTxtC[i].BackColor = Color.Red;
                animation = true;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            {
                for (int i = 0; i < 4; i++)
                    LTxtC[i].ForeColor = Color.Black;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            n1 = ra.Next(0, 10);
            txt1.Text = n1.ToString();
            do
                n2 = ra.Next(0, 10);
            while (n2 == n1);
            txt2.Text = n2.ToString();
            do
                n3 = ra.Next(0, 10);
            while (n3 == n2 || n3 == n1);
            txt3.Text = n3.ToString();

            do
                n4 = ra.Next(0, 10);
            while (n4 == n1 || n4 == n2 || n4 == n3);
            txt4.Text = n4.ToString();
            LTxtC.Add(txt1);
            LTxtC.Add(txt2);
            LTxtC.Add(txt3);
            LTxtC.Add(txt4);
            LTxtP.Add(txtb1);
            LTxtP.Add(txtb2);
            LTxtP.Add(txtb3);
            LTxtP.Add(txtb4);
            
        }

        
        public Form4()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
