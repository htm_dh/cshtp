﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tp1ex2
{
    

    public partial class Form3 : Form
    {
        const int n = 5;
        float[] t = new float[n];
        int i = 0;

        public Form3()
        {
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            float val;
            if (float.TryParse(textBox1.Text,out val) && val >= 0 && val <=20)
            {
                label2.Text = "Entrez la note de l'eleve N° :" + (i + 2).ToString();
                label3.Text = label3.Text + textBox1.Text + " | ";
                t[i] = val;
                i++;
                textBox1.Text = "";
                textBox1.Focus();
                if (i==n)
                {
                    button2.Enabled = true;
                    button3.Enabled = true;
                    textBox1.Enabled = false;
                    button2.Focus();
                }
            }
            else
                MessageBox.Show("vérifier la note entrer !", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            float moy = 0;
           for (int i=0; i<=n;i++)
            {
                moy += t[i];
            }
            textBox2.Text =moy.ToString();
              
        }
    }
}
